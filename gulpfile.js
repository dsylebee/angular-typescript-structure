// require gulp
var gulp = require('gulp');

// require plugins
var concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    rimraf = require('gulp-rimraf'),
    ngAnnotate = require('gulp-ng-annotate'),
    sourcemaps = require('gulp-sourcemaps');

var htmlCssWatch = ['app/index.html', 'theme.css', 'app/**/*.html'];
var jsWatch = ['app/**/*.js', 'app/*.js'];

var vendors = [
    {
        dest: 'www/vendors/angular',
        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular/angular.min.js',
            'bower_components/angular/angular.min.js.map',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            'bower_components/angular-translate/angular-translate.min.js'
        ]
    },
    {
        dest: 'www/vendors/bootstrap',
        files: [
            'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/bootstrap/dist/js/bootstrap.min.js'
        ]
    },
    {
        dest: 'www/vendors/jquery',
        files: [
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/jquery/dist/jquery.min.map'
        ]
    },
    {
        dest: 'www/vendors/font-awesome/css',
        files: [
            'bower_components/font-awesome/css/font-awesome.min.css'
        ]
    },
    {
        dest: 'www/vendors/font-awesome/fonts',
        files: [
            'bower_components/font-awesome/fonts/FontAwesome.otf',
            'bower_components/font-awesome/fonts/fontawesome-webfont.eot',
            'bower_components/font-awesome/fonts/fontawesome-webfont.svg',
            'bower_components/font-awesome/fonts/fontawesome-webfont.ttf',
            'bower_components/font-awesome/fonts/fontawesome-webfont.woff'
        ]
    },
    {
        dest: 'www/vendors/general',
        files: [
            'bower_components/html5shiv/dist/html5shiv.min.js',
            'bower_components/respond/dest/respond.min.js'
        ]
    }
];

function copyHTML() {
    gulp.src('app/modules/**/*.html').pipe(gulp.dest('www/app/modules'));   // move the html files.
    gulp.src('app/index.html').pipe(gulp.dest('www'));      	// move the index.html
    gulp.src('app/theme.css').pipe(gulp.dest('www'));           // move the theme.css
}

function moveAssets() {
    // move the assets
    gulp.src('app/assets/**/*')
        .pipe(gulp.dest('www'));
}

function moveVendors() {

    for(var i = 0 ; i < vendors.length; i++) {
        gulp.src(vendors[i].files)
            .pipe(gulp.dest(vendors[i].dest));
    }
}

function cleanWWW() {
    return gulp.src('www').pipe(rimraf());
}

function compileJS() {
    return gulp.src(['app/main.js', 'app/**/*.js'])
        .pipe(concat('all.js', {newLine: ";\n"}))
        .pipe(gulp.dest('www'))
        .pipe(sourcemaps.init())
        .pipe(ngAnnotate({add:true}))
        .pipe(uglify({mangle: true}))
        .pipe(rename('all.min.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('www'));
}


// tasks.
gulp.task('movesource', ['clean'], function() {
    moveAssets();   // move assets.
    moveVendors();  // vendors.
    copyHTML();     // copy html.
});

gulp.task('watch', function() {
    gulp.watch(htmlCssWatch, ['copy-html']);
    gulp.watch(jsWatch, ['compilejs']);
});

gulp.task('compilejs', compileJS);
gulp.task('clean', cleanWWW);
gulp.task('copy-html', copyHTML);
gulp.task('live', ['watch']);
gulp.task('default', ['clean', 'compilejs', 'movesource']);