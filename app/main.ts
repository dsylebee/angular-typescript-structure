/**
 * Created by dslebee on 2/25/16.
 */

/// <reference path="references.d.ts" />

module poc.application
{
    class Configuration
    {
        static $inject = ['$urlMatcherFactoryProvider', '$urlRouterProvider'];

        constructor($urlMatcherFactoryProvider: angular.ui.IUrlMatcherFactory, $urlRouterProvider: angular.ui.IUrlRouterProvider) {

            // set not case sensitive
            // new in angular ui-router :)
            $urlMatcherFactoryProvider.caseInsensitive(true);
            $urlMatcherFactoryProvider.strictMode(false);

            // re-direct to the main page.
            $urlRouterProvider.otherwise(function($injector, $location) {
                if ($location.$$path === "")
                    return '/';
                else
                    return 'error';
            });
        }
    }

    // create application and set the configuration.
    angular
        .module('poc', ['ui.router'])
        .config(Configuration);
}