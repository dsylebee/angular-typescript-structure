/**
 * Created by dslebee on 2/25/16.
 */
///<reference path="../../references.d.ts" />

module poc.controllers
{
    class Controller
    {
        constructor() {

        }
    }

    angular.module('poc')
        .controller('AboutController', Controller)
        .config(['$stateProvider', function ($stateProvider: angular.ui.IStateProvider) {
            $stateProvider
                .state('about', {
                    url: '/about',
                    templateUrl: 'app/modules/about/about.html',
                    controller: 'AboutController',
                    controllerAs: 'vm'
                });
        }]);
}