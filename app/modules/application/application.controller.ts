/**
 * Created by dslebee on 2/25/16.
 */
/// <reference path="../../references.d.ts" />

module poc.controllers
{
    class Controller
    {
        static $inject = ['$rootScope', '$state'];

        $rootScope: angular.IRootScopeService & any;
        $state: angular.ui.IStateService;

        constructor($rootScope, $state) {

            // set root scope field.
            this.$rootScope = $rootScope;
            this.$state = $state;

            // hook route events.
            this.hookRouteEvents();
        }

        private hookRouteEvents() {

            var that =this;

            // events.
            this.$rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
                console.log(event, toState, toParams, fromState, fromParams);
            });

            this.$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
                that.$rootScope.lastErrorMessage = error;
                that.$state.go('error');
                console.log(event, toState, toParams, fromState, fromParams, error);
            });
        }
    }

    // register the controller
    angular.module('poc').controller('ApplicationController', Controller);
}