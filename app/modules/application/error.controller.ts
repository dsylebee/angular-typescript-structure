/**
 * Created by dslebee on 2/25/16.
 */
/// <reference path="../../references.d.ts"/>

module poc.controllers {

    class Controller
    {
        static $inject = ['$rootScope'];

        public message: string;

        constructor($rootScope) {

            if ($rootScope.lastErrorMessage)
                this.message = $rootScope.lastErrorMessage;
            else
                this.message = 'View not found';
        }
    }

    angular.module('poc')
        .controller('ErrorController', Controller)
        .config(['$stateProvider', function ($stateProvider: angular.ui.IStateProvider) {
            $stateProvider
                .state('error', {
                    url: '/error',
                    template: '<div class="well">{{vm.message}}<div>',
                    controller: 'ErrorController',
                    controllerAs: 'vm'
                });
        }]);
}