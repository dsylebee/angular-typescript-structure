/**
 * Created by dslebee on 2/25/16.
 */
///<reference path="../../references.d.ts" />

module poc.controllers
{
    class Controller
    {
        public welcomeMessage = 'Hello new person! how are you today?';

        constructor() {

        }
    }

    angular.module('poc')
        .controller('HomeController', Controller)
        .config(['$stateProvider', function ($stateProvider: angular.ui.IStateProvider) {
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'app/modules/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                });
        }]);
}